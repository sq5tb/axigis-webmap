import getParamByName from 'get-param-by-name';
import $ from 'jquery';
import {
  in_array
} from './js/aximap';

var param = getParamByName('m', window.location.href);

const projects = {
  'szlak-piastowski': 'Szlak Piastowski',
  'radom-mpu': 'Radom MPZP',
  'mot' : 'Motorowa Odznaka Turystyczna',
  'rajgrod' : 'Rajgród - mapa turystyczna',
  'demo': 'Mapa testowa'
}


if (in_array(param, Object.keys(projects))) {
  var m = require('./js/aximap');
  m.createMap('./maps/' + param);
} else {
  var div = $('#map');
  var ul = document.createElement('ul');

  for (var id in projects) {
    var li = document.createElement('li');
    var a = document.createElement('a');
    a.setAttribute('href', '?m=' + id);
    a.innerText = projects[id];
    li.append(a);
    ul.append(li);
  }
  div.html('');
  div.append(ul);
}
